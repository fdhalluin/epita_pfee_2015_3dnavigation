﻿using UnityEngine;
using System.Collections;

public class Propeller : MonoBehaviour {

	public float Throttle = 0f;
	public float Thrust = 5f;
    public float Torque = 0.1f;

	public float Max_throttle = 5f;
	


	void Start () {
	}


	void FixedUpdate() {
		Throttle = Mathf.Clamp(Throttle, 0.0f, Max_throttle);
		Rigidbody rigidBody = GetComponent<Rigidbody>();
		rigidBody.AddRelativeForce(Vector3.up * Thrust * Throttle);
        rigidBody.AddRelativeTorque(Vector3.up * Torque * Throttle);
	}
}
