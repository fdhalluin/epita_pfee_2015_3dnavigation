﻿using UnityEngine;
using System.Collections;

public class CameraBehaviour : MonoBehaviour {

	public int speed = 5;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		
		transform.position += movement * speed;
	
	}
}
