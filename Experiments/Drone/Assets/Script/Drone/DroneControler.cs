﻿using UnityEngine;
using System.Collections;

public class DroneControler : MonoBehaviour {
	
	public GameObject object_withhelicescontroller;
	public GameObject chassis;
	
	public float ANGLE_MOVEMENT_FORCE = 30.0f;

	public float param_P = 1.0f;
	public float param_D = 20.0f;
	public float param_seuil = 0.6f;

	private Drone drone;

	private Vector3 angles_previous;
	
	// Use this for initialization
	void Start () {
		drone = object_withhelicescontroller.GetComponent<Drone> ();
		angles_previous = chassis.transform.rotation.eulerAngles;
	}

	float getRollPitchOffset(float diff_wanted, float delta_lasttime)
	{
		diff_wanted = (diff_wanted + 180) % 360 - 180;
		delta_lasttime = (delta_lasttime + 180) % 360 - 180;

		float rollpitch = Mathf.Clamp ((param_P * diff_wanted + param_D * delta_lasttime)/200, -param_seuil, param_seuil); 

			//Mathf.Sin (Mathf.Deg2Rad * diff_wanted);
		Debug.Log (" wanted : " +  diff_wanted + ", delta_lasttime " + delta_lasttime + "; -> pitch got "+  rollpitch);
		return rollpitch;
	}

	// Update is called once per frame
	void Update () {
		//onlty le pitch and row control hear. rest in Drone
		UnityEngine.Debug.Log (chassis.transform.rotation.eulerAngles);
		
		
		Vector3 angles = chassis.transform.rotation.eulerAngles;
		
		
		
		int roll_movement = 0, pitch_movement = 0, up_movement = 0, yaw_movement = 0;
		if (Input.GetKey (KeyCode.LeftArrow))
			roll_movement = +1;
		if (Input.GetKey (KeyCode.RightArrow))
			roll_movement = -1;
		if (Input.GetKey (KeyCode.UpArrow))
			pitch_movement = +1;
		if (Input.GetKey (KeyCode.DownArrow))
			pitch_movement = -1;

		
		if (Input.GetKey(KeyCode.R))
			Application.LoadLevel(Application.loadedLevel);
		
		float roll_angle_wanted = angles.z - (roll_movement * ANGLE_MOVEMENT_FORCE);
		float pitch_angle_wanted = angles.x - (pitch_movement * ANGLE_MOVEMENT_FORCE);



		
		// 30f;

		drone.rollRotationOffset = - getRollPitchOffset (roll_angle_wanted, angles.z - angles_previous.z);//- Mathf.Sin(Mathf.Deg2Rad * roll_angle_wanted) * 1.0f ;
		drone.pitchRotationOffset = - getRollPitchOffset (pitch_angle_wanted, angles.x - angles_previous.x) ;

		angles_previous = angles;
	}
}
