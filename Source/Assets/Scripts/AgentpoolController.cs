﻿using UnityEngine;
using System.Collections;

public class AgentpoolController : MonoBehaviour {


	public GameObject agent_prefab;
	public GameObject agent_prefab_real;
	public GameObject destination_pool;
	public GameObject station_pool;

	public static int number_agent = 30;

	public static float frequency_apparitions = 3.0f;

	private ArrayList agents;
	public ArrayList Agents { /* getter */
		get {
			return agents;
		}
	}

	private float last_appears = -1.0f;

	// Create a timer with a two second interval.

	// Hook up the Elapsed event for the timer. 


	System.Random rnd;

	// Use this for initialization
	void Start () {

		agents = new ArrayList();
		
		rnd = new System.Random();

		last_appears = Time.fixedTime;
		/*
		for (int i = 0; i< 0; i++)//to change
		{
			GameObject agent = GameObject.Instantiate(agent_prefab);
			agent.GetComponent<NavmeshAgentManager>().manager = this;

			agent.transform.position = get_station_point();

			agents[i] = agent;
		}*/
	}

	public void make_another_appears()
	{
		GameObject agent = GameObject.Instantiate (agent_prefab_real);
		agent.GetComponent<PhysicalDroneAgent>().manager = this;

		agent.GetComponent<DroneControler> ().reset_starttoposition (get_station_point(45)); //position to a station at height 20

		Vector3 target = get_new_target_for_agent (agent.GetComponent<PhysicalDroneAgent> ());
		agent.GetComponent<PhysicalDroneAgent>().setDestination(target);

		agents.Add (agent);
	}

	public void deleteDrone (GameObject drone)
	{	
		int nb_previous = agents.Count;
		agents.Remove (drone);

		Debug.Log("remove "+ nb_previous + " -> " + agents.Count);
	}

	private Vector3 get_station_point(int y) // by default y is set to 0
	{
		int nb_provenance_points = station_pool.transform.childCount;
		int i_destination = rnd.Next(nb_provenance_points);
		Transform provenance = station_pool.transform.GetChild(i_destination);

		Vector3 position_station = provenance.position;
		position_station.y = y;

		return position_station;
	}

	/* function for old drones. Not used  now. See the one taking a NavmeshAgentManager_newdrone paramater */
	public Vector3 get_new_target_for_agent(NavmeshAgentManager agent)
	{
		if (agent.havePackage()) {
			agent.setHavePackage(false);
			return get_station_point(0);
		} else {
			agent.setHavePackage(true);

			int nb_delivery_points = destination_pool.transform.childCount;
			int i_destination = rnd.Next(nb_delivery_points);
			
			Transform arrival = destination_pool.transform.GetChild(i_destination);
			
			return arrival.position;
		}
	}
	
	public Vector3 get_new_target_for_agent(PhysicalDroneAgent agent)
	{
		// Not assignement not working yet

		if (agent.HavingPackage) {
			agent.setHavingPackage(false);
			return get_station_point(0);
		} else {

			agent.setHavingPackage(true);

			int nb_delivery_points = destination_pool.transform.childCount;
			int i_destination = rnd.Next(nb_delivery_points);
			
			Transform arrival = destination_pool.transform.GetChild(i_destination);
			
			return arrival.position;
		}
	}

	void Update()
	{
		if (Time.fixedTime - last_appears > frequency_apparitions) {
			if (agents.Count < number_agent)
				make_another_appears();
			last_appears = Time.fixedTime;
		}
	}

}
