using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Score : MonoBehaviour
{
	public static float startTime;
	
	void Start ()
	{
		//startTime = Time.timeSinceLevelLoad;
		Time.timeScale = 1.0f;
		startTime = 180;
		NavmeshAgentManager.nb_package_delivered = 0;
		NavmeshAgentManager.colisionsDrones = 0;
		NavmeshAgentManager.colisionsWalls = 0;
	}
	

	void Update ()
	{
		//startTime = startTime + Time.deltaTime;
		startTime = startTime - Time.deltaTime;
		var taggedGameObjects = GameObject.FindGameObjectsWithTag("timeInfo");
		foreach (var Obj in taggedGameObjects)
		{
			Obj.GetComponent<Text>().text = "Timer :  " + Mathf.Floor(startTime).ToString();
		}

		var taggedGameObjects_score = GameObject.FindGameObjectsWithTag("scoreInfo");
		foreach (var Obj in taggedGameObjects_score)
		{
			Obj.GetComponent<Text>().text = "Score :  " + NavmeshAgentManager.nb_package_delivered.ToString();
		}

		var taggedGameObjects_colwalls = GameObject.FindGameObjectsWithTag("colides_walls");
		foreach (var Obj in taggedGameObjects_colwalls)
		{
			Obj.GetComponent<Text>().text = "Walls col. :  " + NavmeshAgentManager.colisionsWalls.ToString();
		}

		var taggedGameObjects_coldrones = GameObject.FindGameObjectsWithTag("colides_drones");
		foreach (var Obj in taggedGameObjects_coldrones)
		{
			Obj.GetComponent<Text>().text = "Dr. col. :  " + (NavmeshAgentManager.colisionsDrones / 2).ToString();
		}




		if (startTime <= 1)
		{
			//Application.Quit();
			Time.timeScale = 0.0f;
		}
	
	}
}