﻿using UnityEngine;
using System.Collections;

public class NavmeshAgentManager : MonoBehaviour {

	public Vector3 position_target;
	public AgentpoolController manager;
	public float Stay_time = 5.0f;

	public float rise_speed = 0.20f;

	public float height_wanted;

	public static int nb_package_delivered;
	public static int colisionsDrones;// !! when 2 drones colisions, colisionsDrones is incrimented by 2 (not 1)
	public static int colisionsWalls;

	private NavMeshAgent agent;

	private bool have_package = false;
	public bool get_have_package(){
		return have_package;
	}

	protected float start_staying = -1.0f;

	void Start () {
		agent = GetComponent<NavMeshAgent> ();
		position_target = manager.get_new_target_for_agent(this);
		nb_package_delivered = 0;
		colisionsDrones = 0;
		colisionsWalls = 0;
	}

	private void update_package()
	{
		/* manage package */
		GameObject package_object = this.transform.GetChild(0).GetChild(0).gameObject;
		package_object.SetActive (this.have_package); //plop
		
		if (!agent.pathPending && agent.remainingDistance < 5) {
			
			if (start_staying < 0.0)
				start_staying = Time.fixedTime;
			else if (start_staying < Time.fixedTime - Stay_time)
			{
				position_target = manager.get_new_target_for_agent(this);
				start_staying = -1.0f;
			}
		}
	}

	void Update () {
		NavMeshAgent drone_associated = this.GetComponent<NavMeshAgent>();
		
		agent.updatePosition = false;
		float speed = Mathf.Min (rise_speed * Mathf.Abs (drone_associated.baseOffset - height_wanted), rise_speed);
		int direction_speed = (drone_associated.baseOffset > height_wanted) ? -1 : 1;
		drone_associated.baseOffset += speed * (float)direction_speed * Time.deltaTime;
		agent.updatePosition = true;

		height_wanted = this.havePackage() ? 10 : 20;

		agent.SetDestination (position_target);

		update_package ();
	}

	public void setHavePackage(bool have_package)
	{
		if (!have_package && this.have_package) {//if we want to drop the package
			Transform package_transform = this.transform.GetChild(0).GetChild(0);

			GameObject new_game_object = (GameObject)Instantiate(package_transform.gameObject, package_transform.position, package_transform.rotation);
			new_game_object.SetActive(false);
			new_game_object.transform.parent = package_transform.parent;
			new_game_object.transform.localScale = package_transform.localScale;

			package_transform.parent = null;
			Rigidbody gameObjectsRigidBody = package_transform.gameObject.AddComponent<Rigidbody>(); // Add the rigidbody.
			gameObjectsRigidBody.mass = 5; // Set the GO's mass to 5 via the Rigidbody.

			//up 45,20,180
			//NavMeshAgent drone_associated = this.GetComponent<NavMeshAgent>();
			//drone_associated.baseOffset -= 10;
			agent.areaMask += 4;
			agent.updatePosition = false;
			//agent.transform.position = new Vector3(45, 20, 180);
			//agent.transform.position = new Vector3(agent.transform.position.x, agent.transform.position.y + 10, agent.transform.position.z);
			//agent.updatePosition = true;

			++nb_package_delivered;
		}
		if (have_package && !this.have_package) {
			
			/*NavMeshAgent drone_associated = this.GetComponent<NavMeshAgent>();
			drone_associated.baseOffset += 10;
			agent.areaMask -= 4;
			agent.updatePosition = false;
			agent.transform.position = new Vector3(agent.transform.position.x, agent.transform.position.y - 10, agent.transform.position.z);
			agent.updatePosition = true;*/
		}
		this.have_package = have_package;
	}

	public bool havePackage()
	{
		return this.have_package;
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.green;
		//Gizmos.DrawLine (transform.position, position_target);
		if (agent.path.corners.Length > 1)
			Gizmos.DrawLine (agent.path.corners [1], agent.path.corners [0]);
	}
}
