using UnityEngine;
using System.Collections;

public class Drone : MonoBehaviour
{

	//start maj : should be not modify, or only in Unity. / start by min, are modified by others class
	public Propeller FrontLeftPropeller;
	public Propeller FrontRightPropeller;
	public Propeller BackLeftPropeller;
	public Propeller BackRightPropeller;
    
    public float throttle_offset = 0f;
    public float Throttle_default = 0.59f;
    public float ControlSensitivity = 0.1f;

	public float yawRotationOffset = 0.0f;
	public float rollRotationOffset = 0.0f;
	public float pitchRotationOffset = 0.0f;

	void Update()
	{
        float dt = Time.deltaTime;
		/*
		throttle_offset = 0;
		rollRotationOffset = 0;
		pitchRotationOffset = 0;
		yawRotationOffset = 0;

        if (Input.GetKey (KeyCode.W))
			throttle_offset = ControlSensitivity;
        if (Input.GetKey (KeyCode.S))
			throttle_offset = - ControlSensitivity;
        //Throttle = Mathf.Clamp01(Throttle);
        
       /* FrontLeftPropeller.Throttle = Throttle;
        FrontRightPropeller.Throttle = Throttle;
        BackLeftPropeller.Throttle = Throttle;
        BackRightPropeller.Throttle = Throttle;
                
		if (Input.GetKey(KeyCode.LeftArrow)) {

			rollRotationOffset = +ControlSensitivity;
        }
		if (Input.GetKey(KeyCode.RightArrow)) {
			rollRotationOffset = -ControlSensitivity;
        }
		if (Input.GetKey(KeyCode.UpArrow)) {
			pitchRotationOffset = +ControlSensitivity;
        }
		if (Input.GetKey(KeyCode.DownArrow)) {
			pitchRotationOffset = -ControlSensitivity;
        }


		*/
		yawRotationOffset = 0;
		throttle_offset = 0;

		//seulement le yaw et le throttle control hear. rest in controller
		
		if (Input.GetKey (KeyCode.W))
			throttle_offset = ControlSensitivity;
		if (Input.GetKey (KeyCode.S))
			throttle_offset = - ControlSensitivity;
		if (Input.GetKey(KeyCode.A)) {
			yawRotationOffset = +ControlSensitivity * 5;
		}
		if (Input.GetKey (KeyCode.D)) {
			yawRotationOffset = -ControlSensitivity * 5;
		}


		float yawOffset = yawRotationOffset / BackLeftPropeller.Torque;
		
		BackLeftPropeller.Throttle = Throttle_default + throttle_offset + pitchRotationOffset/2 - rollRotationOffset/2 + yawOffset;
		BackRightPropeller.Throttle = Throttle_default+ throttle_offset + pitchRotationOffset/2 + rollRotationOffset/2 - yawOffset;
		FrontLeftPropeller.Throttle = Throttle_default + throttle_offset +  - pitchRotationOffset/2 - rollRotationOffset/2 - yawOffset;
		FrontRightPropeller.Throttle = Throttle_default + throttle_offset - pitchRotationOffset/2 + rollRotationOffset/2 + yawOffset;


		if (Input.GetKey (KeyCode.R))
			Application.LoadLevel(Application.loadedLevel);
	}
}
