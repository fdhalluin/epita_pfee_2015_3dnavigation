using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

[ExecuteInEditMode]
public class CameraManager : MonoBehaviour {

    // Attributes relative to the camera following a drone
    public bool following = false;
    public GameObject target = null;
    public Vector3 targetLastPosition;
	public Vector3 targetPosition = new Vector3(0, 120, 0);
	public Vector3 targetAngle = new Vector3(90, 0, 0);
    public float targetSpeedMagnitude = 0;
	public GameObject Target {
		get {
			return target;
		}
	}

	public float cameraSpeed = 1;
	public float rotateSpeed = 1;
	public float cameraFactor = 0.2f;
	public float rotateFactor = 0.1f;
	public float moveSpeed = 8;
	public float zoomSpeed = 60;
	public float hitRadius = 300;
	public float minZoom = 20;
	public float maxZoom = 200;
	public float currentZoom;
	public float targetZoom;
	public AnimationCurve zoomCurve;

	public GameObject UIPanel;
	public GameObject select;

	public bool userControl = false; //when set to true, the user controls the drone selected (should not be true if following to false)

	void Start () {
		UIPanel.SetActive (false);
	}


	void Update () {
		// on click
		if (Input.GetMouseButtonDown (0) && !EventSystem.current.IsPointerOverGameObject()) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			var nearestDistanceSqr = Mathf.Infinity;
			var taggedGameObjects = GameObject.FindGameObjectsWithTag ("Drone"); 
			GameObject nearestObj = null;
			
			if (Physics.Raycast (ray, out hit, 300)) {
				Vector3 destPoint = hit.point;
				
				foreach (var obj in taggedGameObjects) {
					var objectPos = obj.transform.position;
					var distanceSqr = (objectPos - destPoint).sqrMagnitude;
					
					if (distanceSqr < nearestDistanceSqr) {
						nearestObj = obj;
						nearestDistanceSqr = distanceSqr;
					}
				}
			}
			//Drone selection
			if (nearestObj != null && (nearestDistanceSqr < hitRadius || !following)) { // select
				target = nearestObj;
				targetAngle = new Vector3 (60, 0, 0);
				targetLastPosition = target.transform.position;
				following = true;
				UpdateGUIOnce();
			} else {
				if (following) { // unselect
					Unselect();
				}
			}
		// no click
		} else {
			if (!following) {
				Controls ();
				select.transform.position = new Vector3(0,-1,0);
			} else {
				Vector3 targetSpeed = target.transform.position - targetLastPosition;
				targetSpeedMagnitude = targetSpeed.magnitude;
				targetPosition = Utils.Damp (targetPosition, target.transform.position + new Vector3 (0, 20, -11) + targetSpeed * 10, 0.1f, Time.deltaTime, 1);
				targetLastPosition = target.transform.position;
				select.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z);
				select.transform.Rotate (new Vector3(0, 0, 2));

				if (Input.GetKeyDown (KeyCode.Space))
						userControl = !userControl;
					
				//Debug.Log("user control " + userControl);
			}
		}

		Move ();
		UpdateGUI ();
	}

	private void Unselect(){
		Unfocus ();
		following = false;
		userControl = false;
		UpdateGUIOnce ();
	}

	void Move(){
        var dt = Time.deltaTime;
        if (dt > 0)
        {
            transform.position = Utils.Damp(transform.position, targetPosition, cameraFactor, Time.deltaTime, cameraSpeed);
            transform.eulerAngles = Utils.Damp(transform.eulerAngles, targetAngle, rotateFactor, Time.deltaTime, rotateSpeed);
        }
        else
        {
            transform.position = targetPosition;
            transform.eulerAngles = targetAngle;
        }
    }

	void Unfocus (){
		targetPosition.y = targetZoom;
		targetPosition.z = targetPosition.z + 11;
		targetAngle = new Vector3(90, 0, 0);
	}

	void Controls() {
		float percentHeight = targetPosition.y/maxZoom;

		if (!following) {
			if (Input.GetKey (KeyCode.LeftArrow)) {
				targetPosition = targetPosition + new Vector3 (-moveSpeed * percentHeight, 0, 0);
			}
			if (Input.GetKey (KeyCode.RightArrow)) {
				targetPosition = targetPosition + new Vector3 (moveSpeed * percentHeight, 0, 0);
			}
			if (Input.GetKey (KeyCode.UpArrow)) {
				targetPosition = targetPosition + new Vector3 (0, 0, moveSpeed * percentHeight);
			}
			if (Input.GetKey (KeyCode.DownArrow)) {
				targetPosition = targetPosition + new Vector3 (0, 0, -moveSpeed * percentHeight);
			}
		} 
		
		targetZoom = Mathf.Clamp01 (targetZoom - zoomSpeed * Input.GetAxis ("Mouse ScrollWheel"));
		
		targetPosition.y = Mathf.Lerp (minZoom, maxZoom, zoomCurve.Evaluate (targetZoom));
		//transform.position = new Vector3 (transform.position.x, targetPosition.y, transform.position.z);
	}

	void UpdateGUI(){
		if (following) {
			var taggedGameObjects = GameObject.FindGameObjectsWithTag ("heightInfo");
			foreach (var Obj in taggedGameObjects) {
				var v = target.transform.position;
				Obj.GetComponent<Text> ().text = Mathf.RoundToInt(v.y).ToString() + " m";
			}
			taggedGameObjects = GameObject.FindGameObjectsWithTag ("speedInfo");
			foreach (var Obj in taggedGameObjects) {
				Obj.GetComponent<Text> ().text = Mathf.RoundToInt(targetSpeedMagnitude * 60).ToString () +" m/s";
			}
			taggedGameObjects = GameObject.FindGameObjectsWithTag ("statusInfo");
			foreach (var Obj in taggedGameObjects) {
				PhysicalDroneAgent v = target.GetComponentInParent<PhysicalDroneAgent> ();
				string s = v.HavingPackage ? "delivering" : "returning";
				Obj.GetComponent<Text> ().text = s;
			}
			taggedGameObjects = GameObject.FindGameObjectsWithTag ("weightInfo");
			foreach (var Obj in taggedGameObjects) {
				PhysicalDroneAgent v = target.GetComponentInParent<PhysicalDroneAgent> ();
				string s = v.HavingPackage ? "5kg" : "3kg";
				Obj.GetComponent<Text> ().text = s;
			}
		}
	}
	
	void UpdateGUIOnce(){
		if (following) {
			UIPanel.SetActive (true);
		} else {
			UIPanel.SetActive (false);
		}
	}
}
