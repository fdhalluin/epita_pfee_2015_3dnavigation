﻿using UnityEngine;
using System.Collections;

public class PhysicalDroneAgent : MonoBehaviour {
	private static double RVO_EPSILON = 0.0001;
	public static int detect_range = 7, detect_distance = 7;
	public Vector3 position_target;

	public float Stay_time = 5.0f; //time staying at the point to take or deliver a package
	public float distance_to_cible = 10.0f;//distance to the target point needed to consider it have reach it

	protected float start_staying = -1.0f;

	public float calculate_itinary_frequency = 0.5f;//period of recalculation itinary

	public AgentpoolController manager;

	private bool havingPackage = false;
	private DroneControler droneControler;
	private NavMeshPath path;
	private float itinaryCalculatedLastTime = -1.0f;

	public bool HavingPackage {
		get {
			return havingPackage;
		}
	}

	public Rigidbody DroneBody()
	{
		return this.transform.GetComponentsInChildren<Rigidbody> () [0];
	}
	public Vector3 Velocity(){
		return this.DroneBody ().velocity;
	}

	void Start () {
		droneControler = GetComponent<DroneControler> ();
		path = new NavMeshPath();

		itinaryCalculatedLastTime = Time.fixedTime;
	}

	public void setDestination(Vector3 d){
		d.y = 0; // needed otherwise not able to find the destination
		position_target = d;
	}

	private int get_side_from_angle(float angle) // return 0 if the top axis of the drone is the closest to the angle wanted, 1 if it's the left ... until 3
	{
		//Debug.Log ("current angle : " + drone.current_yaw_angle () + ", angle wanted" + angle);
		int side = (((int)(angle - droneControler.current_yaw_angle ()) + 45 + 360) % 360) / 90;
		//Debug.Log ("side : " + side);

		return side;
	}

	private float get_angle_needed() //return the angle of the direction the drone should go in. Path calculated every sec. If -1 shoudl not move
	{
		if (Time.fixedTime > itinaryCalculatedLastTime + calculate_itinary_frequency) {
			Vector3 pos_begin = transform.GetChild(0).position;
			pos_begin.y=0;
			//position du drone, pas du droneController
			bool plop = NavMesh.CalculatePath(pos_begin, position_target, NavMesh.AllAreas, path);

			//sphereCasting ();

			//Debug.Log("calculus successful"+plop);
			itinaryCalculatedLastTime = Time.fixedTime;

		}

		for (int i = 0; i < path.corners.Length-1; i++) { //tmp
			Debug.DrawLine (path.corners [i], path.corners [i + 1], Color.red);
		}

		if (path.corners.Length < 2)
			return -1;

		Vector3 direction_first = path.corners [1] - path.corners [0];
		return getAngleFromVect(direction_first);
	}


	private float getAngleFromVect(Vector3 direction) // return between 0 and 360, direct way. 0 deg correspond with vect (0, 0, 1); 90 with (1, 0, 0)
	{
		Vector3 cross = Vector3.Cross(Vector3.forward, direction);
		float angle_direction_first = Vector3.Angle (Vector3.forward, direction);
		float angle_direction_first_signed = (cross.y < 0)? -angle_direction_first + 360 : angle_direction_first;
		return angle_direction_first_signed;
	}

	void sphereCasting(){


		RaycastHit hit;
		Rigidbody my_body = this.gameObject.GetComponentsInChildren<Rigidbody>()[0];
		Vector3 direction = my_body.velocity;
		//direction.y = 0;//transform.GetChild(0).position.y;
		if (Physics.SphereCast (transform.GetChild(0).position, detect_range, direction, out hit, detect_distance)) {
			//Debug.Log ("maybe colision");
			Vector3 hitspeed = new Vector3(0,0,0);
			if (hit.rigidbody)
				hitspeed = hit.rigidbody.velocity;

			if (my_body && Vector3.Dot (hitspeed,my_body.velocity) < 0 && Vector3.Dot(my_body.velocity, new Vector3(1,0,1)) > 0){
				droneControler.height_wanted = hit.transform.position.y + 10;
			}
		} else {
			if (droneControler.height_wanted > 20)
				droneControler.height_wanted -= 0.1f;
		}
	}

	private void update_package()
	{
		/* manage package */
		GameObject package_object = this.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
		package_object.SetActive (this.havingPackage); //plop

		Vector3 pos_2D = Vector3.ProjectOnPlane(transform.GetChild(0).position, Vector3.up);
		Vector3 dest_2D = Vector3.ProjectOnPlane(position_target, Vector3.up);
		
		if ( Vector3.Distance(pos_2D, dest_2D) < distance_to_cible) {
			Debug.Log("arrive destination");
			if (start_staying < 0.0)
				start_staying = Time.fixedTime;
			else if (start_staying < Time.fixedTime - Stay_time)
			{
				this.setDestination(manager.get_new_target_for_agent(this)); // this function also called after setHavePackage
				start_staying = -1.0f;
			}
			droneControler.height_wanted = this.HavingPackage ? 25: 10;// Drones can also go to 17  and 33
		}




	}

	public void setHavingPackage(bool havingPackage) //called by the manager
	{

		Debug.Log("package asked ? : " +havingPackage);

		if (!havingPackage && this.havingPackage) {//if we want to drop the package

			Transform package_transform = this.transform.GetChild(0).GetChild(0).GetChild(0);
			
			GameObject new_game_object = (GameObject)Instantiate(package_transform.gameObject, package_transform.position, package_transform.rotation);
			new_game_object.SetActive(false);
			new_game_object.transform.parent = package_transform.parent;
			new_game_object.transform.localScale = package_transform.localScale;
			
			package_transform.parent = null;
			Destroy(package_transform.gameObject.GetComponent<FixedJoint>()); // Set the GO's mass to 5 via the Rigidbody.
			
			++NavmeshAgentManager.nb_package_delivered;
		}
		if (havingPackage && !this.havingPackage) {
			
			/*NavMeshAgent drone_associated = this.GetComponent<NavMeshAgent>();
			drone_associated.baseOffset += 10;
			agent.areaMask -= 4;
			agent.updatePosition = false;
			agent.transform.position = new Vector3(agent.transform.position.x, agent.transform.position.y - 10, agent.transform.position.z);
			agent.updatePosition = true;*/
		}
		this.havingPackage = havingPackage;
	}

	void update_drone_to_angle(float angle)
	{
		int side_to_go = get_side_from_angle(angle);

		Vector3 pos_2D = Vector3.ProjectOnPlane(transform.GetChild(0).position, Vector3.up);
		Vector3 beg_2D = Vector3.ProjectOnPlane(droneControler.transform.position, Vector3.up);//position of first apparition of the drone

		//On ne veut ps que le drone puisse se stoper au début
		if (Vector3.Distance (pos_2D, beg_2D) > distance_to_cible && (angle - droneControler.current_yaw_angle () + 360 + 30) % 90 > 60) {//si le drone n'est pas aligne dans une des directions a 30deg pres, on ne le penche pas
			droneControler.pitch_movement = 0;
			droneControler.roll_movement = 0;
		}
		else if (side_to_go == 0) {
			droneControler.pitch_movement = 1;
			droneControler.roll_movement = 0;
		}
		else if (side_to_go == 1) {
			droneControler.pitch_movement = 0;
			droneControler.roll_movement = -1;

		}
		else if (side_to_go == 2) {
			droneControler.pitch_movement = -1;
			droneControler.roll_movement = 0;
		}
		else if (side_to_go == 3) {
			droneControler.pitch_movement = 0;
			droneControler.roll_movement = 1;
		}

		droneControler.yaw_angle_wanted = (angle - side_to_go*90 + 360) % 360;
	}

	void colision_avoidance(){
		ArrayList neighbors = manager.Agents;

		int height_maybe_move;
		if (droneControler.height_wanted <= 17)
			height_maybe_move = (droneControler.height_wanted == 10 ? 17 : 10);
		else
			height_maybe_move = (droneControler.height_wanted == 25 ? 33 : 25);


		int should_stay_votes = 0, should_move_votes = 0;

		Vector3 our_nposition = this.DroneBody().position + this.Velocity () / 2;
		Vector3 our_nposition_2D = Vector3.ProjectOnPlane(our_nposition, Vector3.up);

			//Debug.Log("position itself : "+this.DroneBody().position);
		for (int i = 0; i < neighbors.Count; i++) {
			GameObject neighbor = (GameObject)neighbors[i];
			if (this.gameObject.Equals(neighbor)) { continue; }

			Vector3 neigh_nposition = neighbor.transform.GetComponentsInChildren<Rigidbody>()[0].position + neighbor.transform.GetComponentsInChildren<Rigidbody>()[0].velocity;//better when this one not / 2
			Vector3 neigh_nposition_2D = 	Vector3.ProjectOnPlane(neigh_nposition, Vector3.up);//position of first apparition of the drone


			if (Vector3.Distance(our_nposition_2D, neigh_nposition_2D) <= detect_range){

				DroneControler other_drone = neighbor.GetComponent<DroneControler>();

				if (other_drone.height_wanted == droneControler.height_wanted)
				{

					should_move_votes++;


				}
				else if (other_drone.height_wanted == height_maybe_move)
				{
					should_stay_votes++;
				}
			}
			/*
			Plane plane = new Plane(normal, this.Velocity() + 0.5f * u);
			ORCAplanes.Add(plane);*/
		}

		if(should_move_votes > should_stay_votes)
		{
			Debug.Log("We move to : "+height_maybe_move);
			this.droneControler.height_wanted = height_maybe_move;
		}
	}

	void Update () {

		CameraManager cameraManager = Camera.main.GetComponent<CameraManager> ();
		if (!(cameraManager.userControl && this.transform.GetChild (0).GetChild (0).gameObject == cameraManager.Target))
		{
			float angle = get_angle_needed ();
			float rad = Mathf.Deg2Rad * angle;
			Vector3 direction = new Vector3(Mathf.Sin(rad), 0, Mathf.Cos(rad));

			colision_avoidance ();

			update_drone_to_angle(angle);
		}

		update_package ();

		if (Input.GetKeyDown (KeyCode.O))
			setHavingPackage (false);
		if (Input.GetKeyDown (KeyCode.P))
			setHavingPackage (true);

	}

	void OnDrawGizmos() {
		Gizmos.color = Color.blue;
		//Gizmos.DrawLine (transform.position, position_target);
		if (path.corners.Length > 2) {
			Gizmos.DrawLine (path.corners [0], path.corners [1]);
		}
		//Debug.Log (transform.GetChild(0).position);
		Rigidbody my_body = this.gameObject.GetComponentsInChildren<Rigidbody>()[0];
		Vector3 direction = my_body.velocity;
		/*Vector3 direction = path.corners[1] - transform.GetChild(0).position;
		direction.y = transform.GetChild(0).position.y;*/
		direction = direction / direction.magnitude;
		Gizmos.DrawSphere (transform.GetChild(0).position + direction*detect_distance, detect_range);
	}

	void OnDestroy() {
		manager.deleteDrone (this.gameObject);
	}

	bool linearProgram1(ArrayList planes, int planeNo, Ray ray, float radius, Vector3 optVelocity, bool directionOpt, ref Vector3 result)
	{
		float dotProduct = Vector3.Dot (ray.origin, ray.direction);
		float discriminant = dotProduct * dotProduct + radius * radius - ray.origin.sqrMagnitude;
		
		if (discriminant < 0.0f) {
			/* Max speed sphere fully invalidates line. */
			return false;
		}
		
		float sqrtDiscriminant = Mathf.Sqrt(discriminant);
		float tLeft = -dotProduct - sqrtDiscriminant;
		float tRight = -dotProduct + sqrtDiscriminant;
		
		for (int i = 0; i < planeNo; ++i) {
			Plane pi = (Plane)planes[i];
			Vector3 piPoint = pi.normal.normalized * pi.distance;
			float numerator = Vector3.Dot((piPoint - ray.origin), pi.normal);
			float denominator = Vector3.Dot(ray.direction, pi.normal);
			
			if (denominator * denominator <= RVO_EPSILON) {
				/* Lines line is (almost) parallel to plane i. */
				if (numerator > 0.0f) {
					return false;
				}
				else {
					continue;
				}
			}
			
			float t = numerator / denominator;
			
			if (denominator >= 0.0f) {
				/* Plane i bounds line on the left. */
				tLeft = Mathf.Max(tLeft, t);
			}
			else {
				/* Plane i bounds line on the right. */
				tRight = Mathf.Min(tRight, t);
			}
			
			if (tLeft > tRight) {
				return false;
			}
		}
		
		if (directionOpt) {
			/* Optimize direction. */
			if (Vector3.Dot(optVelocity, ray.direction) > 0.0f) {
				/* Take right extreme. */
				result = ray.origin + tRight * ray.direction;
			}
			else {
				/* Take left extreme. */
				result = ray.origin + tLeft * ray.direction;
			}
		}
		else {
			/* Optimize closest point. */
			float t = Vector3.Dot (ray.direction, (optVelocity - ray.origin));
			
			if (t < tLeft) {
				result = ray.origin + tLeft * ray.direction;
			}
			else if (t > tRight) {
				result = ray.origin + tRight * ray.direction;
			}
			else {
				result = ray.origin + t * ray.direction;
			}
		}
		
		return true;
	}

	bool linearProgram2(ArrayList planes, int planeNo, float radius, Vector3 optVelocity, bool directionOpt, ref Vector3 result)
	{
		Plane p = (Plane)planes [planeNo];
		Vector3 pPoint = p.normal.normalized * p.distance;
		float planeDist = Vector3.Dot (pPoint, p.normal);
		float planeDistSq = planeDist * planeDist;
		float radiusSq = radius * radius;
		
		if (planeDistSq > radiusSq) {
			/* Max speed sphere fully invalidates plane planeNo. */
			return false;
		}
		
		float planeRadiusSq = radiusSq - planeDistSq;
		
		Vector3 planeCenter = planeDist * p.normal;
		
		if (directionOpt) {
			/* Project direction optVelocity on plane planeNo. */
			Vector3 planeOptVelocity = optVelocity - (Vector3.Dot(optVelocity, p.normal)* p.normal);
			float planeOptVelocityLengthSq = planeOptVelocity.sqrMagnitude;
			
			if (planeOptVelocityLengthSq <= RVO_EPSILON) {
				result = planeCenter;
			}
			else {
				result = planeCenter + Mathf.Sqrt(planeRadiusSq / planeOptVelocityLengthSq) * planeOptVelocity;
			}
		}
		else {
			/* Project point optVelocity on plane planeNo. */
			result = optVelocity + (Vector3.Dot((pPoint - optVelocity), p.normal) * p.normal);
			
			/* If outside planeCircle, project on planeCircle. */
			if (result.sqrMagnitude > radiusSq) {
				Vector3 planeResult = result - planeCenter;
				float planeResultLengthSq = planeResult.sqrMagnitude;
				result = planeCenter + Mathf.Sqrt(planeRadiusSq / planeResultLengthSq) * planeResult;
			}
		}
		
		for (int i = 0; i < planeNo; ++i) {
			Plane pi = (Plane)planes [i];
			Vector3 piPoint = pi.normal.normalized * pi.distance;
			if (Vector3.Dot(pi.normal, (piPoint - result)) > 0.0f) {
				/* Result does not satisfy constraint i. Compute new optimal result. */
				/* Compute intersection line of plane i and plane planeNo. */
				Vector3 crossProduct = Vector3.Cross(pi.normal, p.normal);
				
				if (crossProduct.sqrMagnitude <= RVO_EPSILON) {
					/* Planes planeNo and i are (almost) parallel, and plane i fully invalidates plane planeNo. */
					return false;
				}

				Vector3 rayDir = crossProduct.normalized;
				Vector3 lineNormal = Vector3.Cross(rayDir, p.normal);
				Ray ray = new Ray(pPoint + (Vector3.Dot((piPoint - pPoint), pi.normal) / Vector3.Dot(lineNormal, pi.normal)) * lineNormal, rayDir);

				if (!linearProgram1(planes, i, ray, radius, optVelocity, directionOpt, ref result)) {
					return false;
				}
			}
		}
		
		return true;
	}

	int linearProgram3(ArrayList planes, float radius, Vector3 optVelocity, bool directionOpt, ref Vector3 result)
	{
		if (directionOpt) {
			/* Optimize direction. Note that the optimization velocity is of unit length in this case. */
			result = optVelocity * radius;
		}
		else if (optVelocity.sqrMagnitude > radius * radius) {
			/* Optimize closest point and outside circle. */
			result = optVelocity.normalized * radius;
		}
		else {
			/* Optimize closest point and inside circle. */
			result = optVelocity;
		}
		
		for (int i = 0; i < planes.Count; ++i) {
			Plane p = (Plane)planes[i];
			Vector3 pPoint = p.normal.normalized * p.distance;
			if (Vector3.Dot(p.normal, (pPoint - result)) > 0.0f) {
				/* Result does not satisfy constraint i. Compute new optimal result. */
				Vector3 tempResult = result;
				
				if (!linearProgram2(planes, i, radius, optVelocity, directionOpt, ref result)) {
					result = tempResult;
					return i;
				}
			}
		}
		
		return planes.Count;
	}
}
