PFEE 2015 instructions

1. Clone git repository.
2. Set up your git username and email.
3. Complete authors.txt and license.txt.

Coding:
Use VS or MonoDevelop. If you have a license, VS is better.
Follow the coding style and folder structure used in the sample project.

Unity:
Use Unity 5. Learn to use the UI, Gizmos and EditorWindows before you start working on the demo (will save you a ton of debug time).
Put your test projects in Experiments/... (<...> is the root of each project)
Put your final project in Source/ (<Source> is your project root)

Git (Unity projects):
Submit all files in Assets/, including .meta files.
Submit all files in ProjectSettings/.
Do not submit other files (temporary, library, generated, or binary files).
Go easy on assets because your repository size is limited to ~1 Go.

Git (General)
Submit a build for each sprint (zipped) in Builds/
Write proper one-line commit messages (e.g. "[SOTA] Add reference on Spelunky", "[UI] Fix button layout when window is resized").

Screenshots:
Put weekly progress screenshots of experiments and final project in Screenshots/.
Use the Windows Snipping Tool to take screenshots easily.

SOTA:
You can work on a shared google docs or on a document versionned on git.
Put working files and the final PDF in the SOTA/ folder.

EPITA reports:
Put all reports/slides made for EPITA in the EPITA/ folder.
