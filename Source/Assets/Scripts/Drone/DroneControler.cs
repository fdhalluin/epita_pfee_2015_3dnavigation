﻿using UnityEngine;
using System.Collections;

public class DroneControler : MonoBehaviour {
	
	public GameObject object_withhelicescontroller;
	public GameObject chassis;


	public static float aggressivity = 0.5f; // 0 to 1 mapping;tatic float aggressivity = 0; // 0 to 1 mapping;
	public static float speed = 0.5f; // 0 to 1 mapping;


    public AnimationCurve attack_angle_curve;
	public AnimationCurve PIDCoefCurve;
	public float attack_angle = 30.0f;

	public float param_P = 1.0f;
	public float param_D = 20.0f;
	public float param_seuil = 0.6f;

	public float yaw_angle_wanted = 0.0f;
	public float yaw_param_seuil = 10.0f;
	public float yaw_param_P = 1.0f;
	public float yaw_param_D = -19.6f;

	public float height_wanted = 20.0f;

	public float throttle_param_seuil = 3f;//seuil max
	public float throttle_param_seuil_min = -3f;
	public float throttle_param_P = 1.0f;
	public float throttle_param_D = 0.0f;

	private float param_P_reel, param_D_reel, param_seuil_reel, yaw_param_seuil_reel, yaw_param_P_reel, yaw_param_D_reel, throttle_param_seuil_reel, throttle_param_seuil_min_reel, throttle_param_P_reel, throttle_param_D_reel;

	public float manual_throttle_coef = 1.0f;
	public float manual_yawrotation_val = 2.0f;

	public float frequency_pid_update = 0.05f;

	public int roll_movement = 0, pitch_movement = 0;

	private Drone drone;

	private Vector3 angles_previous;
	private Vector3 position_previous;


	private float last_pid_update = 0.0f;
	
	// Use this for initialization
	void Start () {
		drone = object_withhelicescontroller.GetComponent<Drone> ();

		angles_previous = chassis.transform.rotation.eulerAngles;
		position_previous = chassis.transform.position;

	}

	public void reset_starttoposition(Vector3 position)
	{
		this.transform.position = position;

		angles_previous = chassis.transform.rotation.eulerAngles;
		position_previous = chassis.transform.position;
	}

	float getRollPitchOffset(float diff_wanted, float delta_lasttime)
	{
		diff_wanted = (diff_wanted + 360 + 180) % 360 - 180;
		delta_lasttime = (delta_lasttime + 360 + 180) % 360 - 180;

		float rollpitch = Mathf.Clamp ((param_P_reel * diff_wanted + param_D_reel * delta_lasttime)/200, -param_seuil_reel, param_seuil_reel); 

			//Mathf.Sin (Mathf.Deg2Rad * diff_wanted);
		//Debug.Log (" wanted : " +  diff_wanted + ", delta_lasttime " + delta_lasttime + "; -> pitch got "+  rollpitch);
		return rollpitch;
	}

	float getYawOffsetWanted(float diff_wanted, float delta_lasttime)
	{
		diff_wanted = (diff_wanted + 360+ 180) % 360 - 180;
		//Debug.Log ("diff_wanted : " + diff_wanted);

		float yawOffset = Mathf.Clamp((yaw_param_P_reel * diff_wanted + yaw_param_D_reel * delta_lasttime)/200, -yaw_param_seuil_reel, +yaw_param_seuil_reel);
		return yawOffset;
	}

	float getThrottleOffset(float diff_wanted, float delta_lasttime)
	{
		float throttleOffset = Mathf.Clamp((throttle_param_P_reel * diff_wanted + throttle_param_D_reel * delta_lasttime)/10, throttle_param_seuil_min_reel, +throttle_param_seuil_reel);

		//Debug.Log ("diff_wanted : " + diff_wanted + ", calcul : " + throttle_param_P * diff_wanted + throttle_param_D * delta_lasttime);

		return throttleOffset;
	}

	public float current_yaw_angle()
	{
		return angles_previous.y;
	}
	
	void update_pid()
	{

		CameraManager camera_manag = Camera.main.GetComponent<CameraManager> ();

		Vector3 angles = chassis.transform.rotation.eulerAngles;
		Vector3 position = chassis.transform.position;

		if (camera_manag.userControl && this.transform.GetChild(0).GetChild(0).gameObject == camera_manag.Target) {
			roll_movement = 0;
			pitch_movement = 0;

			if (Input.GetKey (KeyCode.UpArrow))
				pitch_movement = +1;
			if (Input.GetKey (KeyCode.DownArrow))
				pitch_movement = -1;
			if (Input.GetKey (KeyCode.LeftArrow))
				roll_movement = 1;
			if (Input.GetKey (KeyCode.RightArrow))
				roll_movement = -1;

			int throttle_direction = 0, yaw_direction = 0;
			if (Input.GetKey (KeyCode.Z))
				throttle_direction = +1;
			if (Input.GetKey (KeyCode.S))
				throttle_direction = -1;
			if(Input.GetKey (KeyCode.D))
			    yaw_direction = 1;
			if (Input.GetKey (KeyCode.Q))
				yaw_direction = -1;


			drone.throttle_offset += throttle_direction * manual_throttle_coef * Time.deltaTime;
			drone.yawRotationOffset = yaw_direction * manual_yawrotation_val * Time.deltaTime;



		} else {


		
			if (Input.GetKey (KeyCode.R))
				Application.LoadLevel (Application.loadedLevel);
		

		
			//yaw rotation
			drone.yawRotationOffset = getYawOffsetWanted (yaw_angle_wanted - angles.y, angles.y - angles_previous.y);
		
			drone.throttle_offset = getThrottleOffset (height_wanted - position.y, position.y - position_previous.y);
		

		}

		attack_angle = attack_angle_curve.Evaluate(speed);
		float roll_angle_wanted = angles.z - (roll_movement * attack_angle);
		float pitch_angle_wanted = angles.x - (pitch_movement * attack_angle);
		
		
		
		drone.rollRotationOffset = - getRollPitchOffset (roll_angle_wanted, angles.z - angles_previous.z);//- Mathf.Sin(Mathf.Deg2Rad * roll_angle_wanted) * 1.0f ;
		drone.pitchRotationOffset = - getRollPitchOffset (pitch_angle_wanted, angles.x - angles_previous.x);

		angles_previous = angles;
		position_previous = position;

	}

	void updadePidParameters()
	{
		float coef = PIDCoefCurve.Evaluate (aggressivity);
		//coef = 1.0f;

		param_P_reel = param_P * coef;
		param_D_reel = param_D * coef;
		param_seuil_reel = param_seuil * coef;

		yaw_param_seuil_reel = yaw_param_seuil * coef;
		yaw_param_P_reel = yaw_param_P * coef;
		yaw_param_D_reel = yaw_param_D * coef;

		throttle_param_seuil_reel = throttle_param_seuil * Mathf.Max(coef, 1.0f);//seuil max
		throttle_param_seuil_min_reel = throttle_param_seuil_min * coef;
		throttle_param_P_reel = throttle_param_P;
		throttle_param_D_reel = throttle_param_D;

	}

	// Update is called once per frame
	void FixedUpdate () {

		if (Time.fixedTime - last_pid_update > frequency_pid_update) {
			updadePidParameters();
			update_pid();
			last_pid_update = Time.fixedTime;
		}

	}




}
