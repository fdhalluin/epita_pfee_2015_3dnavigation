using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Options : MonoBehaviour
{
	GameObject options_panel;
	bool active = false;
		
	void Start()
	{
		options_panel = GameObject.Find ("Options Panel");
		AgentpoolController.number_agent = 100;
		AgentpoolController.frequency_apparitions = 2;
		PhysicalDroneAgent.detect_distance = 0;
        DroneControler.aggressivity = 0.5f;
		DroneControler.speed = 0.5f;
		/*foreach (Transform child in options_panel.transform)
		{
			child.gameObject.SetActive(false);
		}*/
	}
		
	void Update()
	{

		var taggedGameObjects = GameObject.FindGameObjectsWithTag("NbDroneInfo");
		foreach (var Obj in taggedGameObjects)
		{
			Obj.GetComponent<Text>().text = "Number of drones :  " + AgentpoolController.number_agent;
		}
		var taggedDelayGameObjects = GameObject.FindGameObjectsWithTag("DelayInfo");
		foreach (var Obj in taggedDelayGameObjects)
		{
			Obj.GetComponent<Text>().text = "Occurence delay :  " + AgentpoolController.frequency_apparitions;
		}
		var taggedDistanceGameObjects = GameObject.FindGameObjectsWithTag("AggressivityInfo");
		foreach (var Obj in taggedDistanceGameObjects)
		{
			Obj.GetComponent<Text>().text = "Drone aggressivity :  " + (DroneControler.aggressivity*100).ToString("00") + "%";
		}
		var taggedSpeedGameObjects = GameObject.FindGameObjectsWithTag("SpeedSliderInfo");
		foreach (var Obj in taggedSpeedGameObjects)
		{
			Obj.GetComponent<Text>().text = "Drone speed :  " + (DroneControler.speed*100).ToString("00") + "%";
		}

	}

	public void clicked()
	{
		if (active == false) {
			foreach (Transform child in options_panel.transform) {
				child.gameObject.SetActive (true);
			}
			active = true;
		} else {
			foreach (Transform child in options_panel.transform) {
				child.gameObject.SetActive (false);
				active = false;
			}
		}
	}

	public void slide_nb_drone(float new_number_agent)
	{
		AgentpoolController.number_agent = (int) new_number_agent;
	}

	public void slide_frequency_apparition(float new_frequency_apparition)
	{
		AgentpoolController.frequency_apparitions = new_frequency_apparition;
	}

	public void slide_aggressivity(float new_aggressivity)
	{
		DroneControler.aggressivity = new_aggressivity;
	}
	public void slide_speed(float new_speed)
	{
		DroneControler.speed = new_speed;
	}
}