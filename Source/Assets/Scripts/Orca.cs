using System;
using UnityEngine;
using System.Collections;

public class Orca
{
	private static float RVO_EPSILON = 0.00001f;
	
	
	public static bool linearProgram1(ArrayList planes, int planeNo, Ray ray, float radius, Vector3 optVelocity, bool directionOpt, Vector3 result)
	{
		float dotProduct = Vector3.Dot (ray.origin, ray.direction);
		float discriminant = dotProduct * dotProduct + radius * radius - ray.origin.sqrMagnitude;
		
		if (discriminant < 0.0f) {
			/* Max speed sphere fully invalidates line. */
			return false;
		}
		
		float sqrtDiscriminant = Mathf.Sqrt(discriminant);
		float tLeft = -dotProduct - sqrtDiscriminant;
		float tRight = -dotProduct + sqrtDiscriminant;
		
		for (int i = 0; i < planeNo; ++i) {
			Plane pi = (Plane)planes[i];
			Vector3 piPoint = pi.normal.normalized * pi.distance;
			float numerator = Vector3.Dot((piPoint - ray.origin), pi.normal);
			float denominator = Vector3.Dot(ray.direction, pi.normal);
			
			if (denominator * denominator <= RVO_EPSILON) {
				/* Lines line is (almost) parallel to plane i. */
				if (numerator > 0.0f) {
					return false;
				}
				else {
					continue;
				}
			}
			
			float t = numerator / denominator;
			
			if (denominator >= 0.0f) {
				/* Plane i bounds line on the left. */
				tLeft = Mathf.Max(tLeft, t);
			}
			else {
				/* Plane i bounds line on the right. */
				tRight = Mathf.Min(tRight, t);
			}
			
			if (tLeft > tRight) {
				return false;
			}
		}
		
		if (directionOpt) {
			/* Optimize direction. */
			if (Vector3.Dot(optVelocity, ray.direction) > 0.0f) {
				/* Take right extreme. */
				result = ray.origin + tRight * ray.direction;
			}
			else {
				/* Take left extreme. */
				result = ray.origin + tLeft * ray.direction;
			}
		}
		else {
			/* Optimize closest point. */
			float t = Vector3.Dot (ray.direction, (optVelocity - ray.origin));
			
			if (t < tLeft) {
				result = ray.origin + tLeft * ray.direction;
			}
			else if (t > tRight) {
				result = ray.origin + tRight * ray.direction;
			}
			else {
				result = ray.origin + t * ray.direction;
			}
		}
		
		return true;
	}
	
	public static bool linearProgram2(ArrayList planes, int planeNo, float radius, Vector3 optVelocity, bool directionOpt, Vector3 result)
	{
		Plane p = (Plane)planes [planeNo];
		Vector3 pPoint = p.normal.normalized * p.distance;
		float planeDist = Vector3.Dot (pPoint, p.normal);
		float planeDistSq = planeDist * planeDist;
		float radiusSq = radius * radius;
		
		if (planeDistSq > radiusSq) {
			/* Max speed sphere fully invalidates plane planeNo. */
			return false;
		}
		
		float planeRadiusSq = radiusSq - planeDistSq;
		
		Vector3 planeCenter = planeDist * p.normal;
		
		if (directionOpt) {
			/* Project direction optVelocity on plane planeNo. */
			Vector3 planeOptVelocity = optVelocity - (Vector3.Dot(optVelocity, p.normal)* p.normal);
			float planeOptVelocityLengthSq = planeOptVelocity.sqrMagnitude;
			
			if (planeOptVelocityLengthSq <= RVO_EPSILON) {
				result = planeCenter;
			}
			else {
				result = planeCenter + Mathf.Sqrt(planeRadiusSq / planeOptVelocityLengthSq) * planeOptVelocity;
			}
		}
		else {
			/* Project point optVelocity on plane planeNo. */
			result = optVelocity + (Vector3.Dot((pPoint - optVelocity), p.normal) * p.normal);
			
			/* If outside planeCircle, project on planeCircle. */
			if (result.sqrMagnitude > radiusSq) {
				Vector3 planeResult = result - planeCenter;
				float planeResultLengthSq = planeResult.sqrMagnitude;
				result = planeCenter + Mathf.Sqrt(planeRadiusSq / planeResultLengthSq) * planeResult;
			}
		}
		
		for (int i = 0; i < planeNo; ++i) {
			Plane pi = (Plane)planes [i];
			Vector3 piPoint = pi.normal.normalized * pi.distance;
			if (Vector3.Dot(pi.normal, (piPoint - result)) > 0.0f) {
				/* Result does not satisfy constraint i. Compute new optimal result. */
				/* Compute intersection line of plane i and plane planeNo. */
				Vector3 crossProduct = Vector3.Cross(pi.normal, p.normal);
				
				if (crossProduct.sqrMagnitude <= RVO_EPSILON) {
					/* Planes planeNo and i are (almost) parallel, and plane i fully invalidates plane planeNo. */
					return false;
				}
				
				Vector3 rayDir = crossProduct.normalized;
				Vector3 lineNormal = Vector3.Cross(rayDir, p.normal);
				Ray ray = new Ray(pPoint + (Vector3.Dot((piPoint - pPoint), pi.normal) / Vector3.Dot(lineNormal, pi.normal)) * lineNormal, rayDir);
				
				if (!linearProgram1(planes, i, ray, radius, optVelocity, directionOpt, result)) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	public static int linearProgram3(ArrayList planes, float radius, Vector3 optVelocity, bool directionOpt, Vector3 result)
	{
		if (directionOpt) {
			/* Optimize direction. Note that the optimization velocity is of unit length in this case. */
			result = optVelocity * radius;
		}
		else if (optVelocity.sqrMagnitude > radius * radius) {
			/* Optimize closest point and outside circle. */
			result = optVelocity.normalized * radius;
		}
		else {
			/* Optimize closest point and inside circle. */
			result = optVelocity;
		}
		
		for (int i = 0; i < planes.Count; ++i) {
			Plane p = (Plane)planes[i];
			Vector3 pPoint = p.normal.normalized * p.distance;
			if (Vector3.Dot(p.normal, (pPoint - result)) > 0.0f) {
				/* Result does not satisfy constraint i. Compute new optimal result. */
				Vector3 tempResult = result;
				
				if (!linearProgram2(planes, i, radius, optVelocity, directionOpt, result)) {
					result = tempResult;
					return i;
				}
			}
		}
		
		return planes.Count;
	}
}