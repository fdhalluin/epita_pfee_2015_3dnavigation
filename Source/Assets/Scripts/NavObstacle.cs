using UnityEngine;
using System.Collections;

public class NavObstacle : MonoBehaviour {


    System.Random rnd;
    float last_occurence = 0; 
   
	// Use this for initialization
	void Start () {


       rnd = new System.Random();
       GameObject spawn = generateSpawn();
       spawn.SetActive(true);
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Time.fixedTime - last_occurence > 50)
        {

            RemoveActive();

            GameObject spawn = generateSpawn();
            spawn.SetActive(true);

            last_occurence = Time.fixedTime;
        }
	
	}

    public void RemoveActive()
    {
        GameObject[] childsG = new GameObject[transform.childCount];
        int i=0;
        foreach (Transform child in transform)
        {
            childsG[i] = child.gameObject;
            i++;
        }

        foreach (GameObject child in childsG)
        {
             if (child.activeInHierarchy)
                    child.SetActive(false);
        }
    }

    public GameObject generateSpawn()
    {
        //int nb_spawn_points = this.transform.childCount;
        //Debug.Log("points " + nb_spawn_points);
        //int i_spawn = rnd.Next(nb_spawn_points);
		int i_spawn = rnd.Next(3);

        Transform arrival = this.transform.GetChild(i_spawn);
		Transform zone = null;


		switch (arrival.gameObject.name.ToString()) {
		case "Obstacle":
			zone = this.transform.GetChild(3);
			zone.gameObject.SetActive(true);
			break;
		case "Obstacle 1":
			zone = this.transform.GetChild(4);
			zone.gameObject.SetActive(true);
			break;
		case "Obstacle 4":
			zone = this.transform.GetChild(5);
			zone.gameObject.SetActive(true);
			break;
		default:
			break;

		}

		Color color = new Color(220,0,0, 0.3f);
		
		MeshRenderer gameObjectRenderer = zone.gameObject.GetComponent<MeshRenderer>();
		
		Material newMaterial = new Material(Shader.Find("Transparent/Diffuse"));
		
		newMaterial.color = color;
		gameObjectRenderer.material = newMaterial;


        return arrival.gameObject;
    }


}
