using UnityEngine;
using System.Collections;

public class Drone : MonoBehaviour
{

	//start maj : should be not modify, or only in Unity. / start by min, are modified by others class
	public Propeller FrontLeftPropeller;
	public Propeller FrontRightPropeller;
	public Propeller BackLeftPropeller;
	public Propeller BackRightPropeller;
    
    public float throttle_offset = 0f;
    public float Throttle_default = 0.59f;
	public float throttle_sensibility = 2f;

	public float yawRotationOffset = 0.0f;
	public float rollRotationOffset = 0.0f;
	public float pitchRotationOffset = 0.0f;

	private bool had_collision = false;

	void Update()
	{

		float yawOffset = yawRotationOffset / BackLeftPropeller.Torque;
		
		BackLeftPropeller.Throttle = Throttle_default + throttle_offset + pitchRotationOffset/2 - rollRotationOffset/2 + yawOffset;
		BackRightPropeller.Throttle = Throttle_default+ throttle_offset + pitchRotationOffset/2 + rollRotationOffset/2 - yawOffset;
		FrontLeftPropeller.Throttle = Throttle_default + throttle_offset +  - pitchRotationOffset/2 - rollRotationOffset/2 - yawOffset;
		FrontRightPropeller.Throttle = Throttle_default + throttle_offset - pitchRotationOffset/2 + rollRotationOffset/2 + yawOffset;


		if (Input.GetKey (KeyCode.R))
			Application.LoadLevel(Application.loadedLevel);
	}

	
	void Explode() {
		var exp = GetComponent<ParticleSystem>();
		exp.Play();


		Destroy(transform.parent.gameObject, exp.duration);
	}
	
	void OnCollisionEnter(Collision coll) {


		if (had_collision) {
			Debug.Log ("was going to collide two times");
			return;
		}




		if (coll.gameObject.isStatic)
			NavmeshAgentManager.colisionsWalls++;
		else {
			NavmeshAgentManager.colisionsDrones++;

			DroneControler itself_controller = this.GetComponentInParent<DroneControler> ();
			DroneControler other_controller = coll.gameObject.GetComponentInParent<DroneControler> ();

			if (itself_controller.height_wanted == other_controller.height_wanted)
				Debug.Log ("same height during colision" + itself_controller.height_wanted); 
		}

		Debug.Log ("colision !!"); 

		had_collision = true;

		Explode();
	}
}
