using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOver : MonoBehaviour
{
	GameObject game_over;
	private float startTime;

	void Start ()
	{
		game_over = GameObject.Find ("GameOver Panel");
		
	}
	
	
	void Update ()
	{
		if (Score.startTime <= 1) {
			foreach (Transform child in game_over.transform) {
				child.gameObject.SetActive (true);

				var taggedGameObjects_score = GameObject.FindGameObjectsWithTag("gameoverInfo");
				foreach (var Obj in taggedGameObjects_score)
				{
					Obj.GetComponent<Text>().text = "GAME OVER\n \n Score : \n Nb package delivered : "+ NavmeshAgentManager.nb_package_delivered.ToString()
						+ "\n Number of drones collision : " + (NavmeshAgentManager.colisionsDrones / 2).ToString()
							+ "\n Number of walls collision : " + NavmeshAgentManager.colisionsWalls.ToString();

				}
			}
		}

	}
}

