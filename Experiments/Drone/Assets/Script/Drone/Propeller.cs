﻿using UnityEngine;
using System.Collections;

public class Propeller : MonoBehaviour {

	public float Throttle = 0f;
	public float Thrust = 5f;
    public float Torque = 0.1f;

	private Rigidbody rb;
	private Vector3 movement;


	void Start () {
		rb = GetComponent<Rigidbody>();
		movement = new Vector3 (0, 1.0f,0);
	}


	void FixedUpdate() {
        Throttle = Mathf.Clamp01(Throttle);
		Rigidbody rigidBody = GetComponent<Rigidbody>();
		rigidBody.AddRelativeForce(Vector3.up * Thrust * Throttle);
        rigidBody.AddRelativeTorque(Vector3.up * Torque * Throttle);
	}
}
