using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {
	
	GameObject pause_panel;
	bool Paused = false;
	
	void Start()
	{
		pause_panel = GameObject.Find ("Pause Panel");
		/*foreach (Transform child in pause_panel.transform)
		{
			child.gameObject.SetActive(false);
		}*/
	}
	
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			if(Paused == false){
				Time.timeScale = 0.0f;
				foreach (Transform child in pause_panel.transform)
				{
					child.gameObject.SetActive(true);
				}
				//Cursor.visible = false;
				//Screen.lockCursor = true;
				Paused = true;
			} else {
				Time.timeScale = 1.0f;
				foreach (Transform child in pause_panel.transform)
				{
					child.gameObject.SetActive(false);
				}
				//Cursor.visible = true;
				//Screen.lockCursor = false;
				Paused = false;
			}
		}
	}

	public void Resume(){
		Time.timeScale = 1.0f;
		foreach (Transform child in pause_panel.transform) {
			child.gameObject.SetActive (false);
		}
	}
}